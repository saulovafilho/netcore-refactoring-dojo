﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseTest
    {
        [Fact]
        public void foo()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Bolo de Mana Conjurado", PrazoParaVenda = 5, Qualidade = 10 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("Bolo de Mana Conjurado", Items[0].Nome);
            Assert.Equal(4, Items[0].PrazoParaVenda); //decrementa o prazo em 1 dia
            Assert.Equal(8, Items[0].Qualidade); //decrementa 2x mais rápido por ser Item Conjurado
        }
    }
}
